import pytest
from api.get_hello_world import hello_world

def test_hello_world():
    expect = hello_world()
    assert expect == {'message':"Hello, world!"}