from mongoengine import Document, StringField, IntField, ListField
# 需要與 db 的 table 名稱一樣(大小寫沒關係)
class Job(Document):    
    CrawlerDate = StringField()
    JobName = StringField()
    JobEdu = StringField()
    JobSal = IntField()
    JobLoc = StringField()
    JobCounty = StringField()
    JobExp = IntField()  
    JobLanguage = StringField()  
    JobKeyword = StringField()     
    JobTool = StringField()    
    JobToolCount = StringField()    
    JobBusinessTrip = StringField()  
    JobUrl = StringField()  

