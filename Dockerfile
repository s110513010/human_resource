FROM python:3.9.7

RUN apt-get update

RUN mkdir /HumanResourceProject
COPY . /HumanResourceProject/
WORKDIR /HumanResourceProject/

# install package
RUN pip install pipenv==2023.2.18 && pipenv sync

# genenv
RUN VERSION=RELEASE python genenv.py
 
# 預設執行的指令
CMD ["pipenv", "run", "uvicorn", "api.main:app", "--host", "0.0.0.0", "--port", "8888"]

# build docker 指令：docker build -f Dockerfile -t xxx/xxx:2.3.9
# -f Dockerfile: 指定 Dockerfile 的檔案路徑。這邊使用的是相對路徑，如果是絕對路徑則不需要 -f 參數。
# -t: 指定新建立的 Docker Image 的名稱及標籤。格式為 [image name]:[tag]，如果不指定 tag，則預設為 latest。