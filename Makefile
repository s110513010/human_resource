GIT_TAG := $(shell git describe --abbrev=0 --tags)


# 建立 docker image
build-image:
	docker build -f Dockerfile -t danielhch/api:${GIT_TAG} .

# 推送 image
push-image:
	docker push danielhch/api:${GIT_TAG}

# 部屬爬蟲
deploy-api:
	GIT_TAG=${GIT_TAG} docker stack deploy --with-registry-auth -c api.yml api
